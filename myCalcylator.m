%MATLAB 2020b

 %name: myCalculator

 %author:Carborundum

 %date: 8.11.2020

 %version: v1.0

%p1

h=6.626e-34; parallel=h/(2*pi) %[h]=[J*s]

%p2

e=exp(1) %defining Euler number

altar=sin((pi/4)/e)

%p3
f=hex2dec('0x0098d6')/(1.445*1e23) %translate hexadecymal to decimal

%p4

sqrt(e-(pi)) % using previously used Euler numbet

%p5

pi12=num2str(pi,15); p5=pi12(14) % print 12 number of Pi

%p6

mr=21*365*24+21*24+4*31*24+2*30*24+8*24+11 %force methot to calculate birh time in hours from my birth to 8XI2020

%p7

r=6371000
a=e^((sqrt(7)/2)-log(r/(10^8))) % using previously used Euler numbet
b=hex2dec('0xaaff') %translate hexadecymal to decimal
c=atan(a/b)

%p8

p=6.02*1E23 %defining Avogadro constant
e=(1/4)*(1E-6)*p

%p9

i=(2*e)/100
j=(i/(2*e))*1000 % carbon isotope in all carbon atoms in per mille
